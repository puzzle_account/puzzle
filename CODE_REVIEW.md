# Code fixes and review

## Code smells

* unsubscribe() never called throughout the application

* Encryption is missing. I can able to EDIT the storage file inside the scratch folder

* Search always showing the previous result until I press enter

* Issue with storage synchronization. Reading list breaks sometime.
  This happens after refreshing the page if I try to remove the reading list item.

## a11y and accessibility issues

* 'Alt' text is missing in the "img" tag

* Contrast color issue in few places

## Improvements

* Loading or progress bar to the search results

* Responsibility can be added to the mobile view

* Auto complete for search

## Fixed the below items

* 'Alt' text is missing in the "img" tag

* Contrast color issue in few places

* Loading or progress bar to the search results

* Responsibility can be added to the mobile view

* "Search always showing the previous result until I press enter", This issue will be resolved by default 
  with Task 1 changes.

* Fixed the test errors
